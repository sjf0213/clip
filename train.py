# -*- coding: utf-8 -*-
# It requires OpenCV installed for Python
import sys
import cv2
import os
from PIL import Image
from sys import platform
from random import shuffle
import numpy as np
import argparse
sys.path.append('/usr/local/python')
from openpose import pyopenpose as op

from keras.models import Sequential
from keras.layers import *
from keras.optimizers import *

def one_hot_label(img_path):
    label = img_path.split('.')[1]
    if label == 'yes':
        ohl = np.array([1,0])
    elif label == 'no':
        ohl = np.array([0,1])
    return ohl

train_data = '/home/jufeng/stuff/images/datademo'
test_data = '/home/jufeng/stuff/images/datatest'
# Flags
parser = argparse.ArgumentParser()
parser.add_argument("--train_image_dir", default="/home/jufeng/stuff/images/train/", help="Process a directory of images. Read all standard formats (jpg, png, bmp, etc.).")
parser.add_argument("--test_image_dir", default="/home/jufeng/stuff/images/test/", help="Process a directory of images. Read all standard formats (jpg, png, bmp, etc.).")
args = parser.parse_known_args()

# y_train = np.loadtxt('/home/jufeng/stuff/images/trainlabel.txt')
# y_test = np.loadtxt('/home/jufeng/stuff/images/testlabel.txt')
# print(y_train.shape)
# print(y_test.shape)

def train_data_with_label():
    train_images = []
    train_image_dir = args[0].train_image_dir
    imagePaths = op.get_images_on_directory(train_image_dir)
    for imagePath in imagePaths:
        img = cv2.imread(imagePath)
        # cv2.imshow("before resize", img)
        # cv2.waitKey(0)
        img = cv2.resize(img, (140, 80))
        # cv2.imshow("after resize", img)
        # cv2.waitKey(0)
        train_images.append([np.array(img), one_hot_label(imagePath)])
        shuffle(train_images)
        # print(train_images.shape())
        return train_images

def test_data_with_label():
    test_images = []
    test_image_dir = args[0].test_image_dir
    imagePaths = op.get_images_on_directory(test_image_dir)
    for imagePath in imagePaths:
        img = cv2.imread(imagePath)
        # cv2.imshow("before resize", img)
        # cv2.waitKey(0)
        img = cv2.resize(img, (140, 80))
        # cv2.imshow("after resize", img)
        # cv2.waitKey(0)
        test_images.append([np.array(img), one_hot_label(imagePath)])
        # print(train_images.shape())
        return test_images

training_images = train_data_with_label()
testing_images = test_data_with_label()
print('training_images:%s'%(type(training_images[0][0]).__name__))
print(training_images[0][0].shape)
tr_img_data = np.array([i[0] for i in training_images]).reshape(-1,140,80,3)
print('tr_img_data:%s'%(type(tr_img_data).__name__))
print(tr_img_data.shape)
tr_lbl_data = np.array([i[1] for i in training_images])
tst_img_data = np.array([i[0] for i in testing_images]).reshape(-1,140,80,3)
tst_lbl_data = np.array([i[1] for i in testing_images])

model = Sequential()
model.add(InputLayer(input_shape=[140, 80, 3]))

model.add(Conv2D(filters=32, kernel_size=5, strides=1, padding='same', activation='relu'))
model.add(MaxPool2D(pool_size=5, padding='same'))

model.add(Conv2D(filters=50, kernel_size=5, strides=1, padding='same', activation='relu'))
model.add(MaxPool2D(pool_size=5, padding='same'))

model.add(Conv2D(filters=80, kernel_size=5, strides=1, padding='same', activation='relu'))
model.add(MaxPool2D(pool_size=5, padding='same'))

model.add(Dropout(0.25))
model.add(Flatten())
model.add(Dense(512, activation='relu'))
model.add(Dropout(rate=0.5))
model.add(Dense(2, activation='softmax'))
optimizer=Adam(lr=1e-3)

model.compile(optimizer=optimizer, loss='categorical_crossentropy', metrics=['accuracy'])
model.fit(x=tr_img_data, y=tr_lbl_data, epochs=10,batch_size=3)
model.summary()

model.save('boobs_model.h5')

# predict
predict_img = '/home/jufeng/stuff/images/boobs/vlcsnap-2019-09-19-19h40m06s971.yes.png'
test1 = cv2.imread(predict_img, 1)
# cv2.imshow("before resize", test1)
# cv2.waitKey(0)
test1 = cv2.resize(test1, (80, 140))
# test1.reshape(1,140,80,3)

predict_rt = model.predict(np.asarray([test1]))
print(predict_rt)