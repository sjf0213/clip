# From Python
# It requires OpenCV installed for Python
import sys
import cv2
import os
import time
from sys import platform
import argparse
from point2d import Point2D
# sys.path.append('../../python');
# If you run `make install` (default path is `/usr/local/python` for Ubuntu), you can also access the OpenPose/python module from there. This will install OpenPose and the python library at your desired installation path. Ensure that this is in your python path in order to use it.
sys.path.append('/usr/local/python')
from openpose import pyopenpose as op

print("sjf0213---parse args")
# Flags
parser = argparse.ArgumentParser()
# parser.add_argument("--image_path", default="/home/jufeng/stuff/images/n3.png", help="Process an image. Read all standard formats (jpg, png, bmp, etc.).")
parser.add_argument("--image_dir", default="/home/jufeng/stuff/shots/", help="Process a directory of images. Read all standard formats (jpg, png, bmp, etc.).")
args = parser.parse_known_args()
# Custom Params (refer to include/openpose/flags.hpp for more parameters)
params = dict()
params["model_folder"] = "/home/jufeng/codes/openpose/models/"
# Add others in path?
for i in range(0, len(args[1])):
    curr_item = args[1][i]
    if i != len(args[1])-1: next_item = args[1][i+1]
    else: next_item = "1"
    if "--" in curr_item and "--" in next_item:
        key = curr_item.replace('-','')
        if key not in params:  params[key] = "1"
    elif "--" in curr_item and "--" not in next_item:
        key = curr_item.replace('-','')
        if key not in params: params[key] = next_item

# Construct it from system arguments
# op.init_argv(args[1])
# oppython = op.OpenposePython()

try:
    # Starting OpenPose
    print("sjf0213---Starting OpenPose")
    opWrapper = op.WrapperPython()
    opWrapper.configure(params)
    opWrapper.start()
    print("sjf0213---Starting OpenPose end")

    # Read frames on directory
    image_dir = args[0].image_dir
    imagePaths = op.get_images_on_directory(image_dir)
    save_dir1 = image_dir + "keys/"
    save_dir2 = image_dir + "body/"
    save_dir3 = image_dir + "boobs/"
    if not os.path.exists(save_dir1):
        os.makedirs(save_dir1)
    if not os.path.exists(save_dir2):
        os.makedirs(save_dir2)
    if not os.path.exists(save_dir3):
        os.makedirs(save_dir3)

    print("Directory image count : %d"%(len(imagePaths)))
    start = time.time()

    # Process and display images
    for imagePath in imagePaths:
        # Find all persons
        datum = op.Datum()
        imageToProcess = cv2.imread(imagePath)
        img_height, img_width = imageToProcess.shape[:2]
        datum.cvInputData = imageToProcess
        opWrapper.emplaceAndPop([datum])
        print("Body keypoints type: \n" + str(type(datum.poseKeypoints).__name__))
        print("Body keypoints shape: \n" + str(datum.poseKeypoints.shape))
        print("Body keypoints: \n" + str(datum.poseKeypoints))
        if len(datum.poseKeypoints.shape) == 0:
            continue
        # Find Max Area person
        p1 = Point2D(0.0, 0.0)
        p2 = Point2D(0.0, 0.0)
        max_p1 = Point2D(0.0, 0.0)
        max_p2 = Point2D(0.0, 0.0)
        areaMax = 0.0
        personIndex = 0
        index = 0
        # print("sjf0213---2")
        print("sjf0213----image size, width: %f, height: %f"%(img_width, img_height))
        for person in datum.poseKeypoints:
            first_init = False
            for dotinfo in person:
                if dotinfo[2] > 0.1:
                    if first_init == False:
                        first_init = True
                        p1.x = dotinfo[0]
                        p2.x = dotinfo[0]
                        p1.y = dotinfo[1]
                        p2.y = dotinfo[1]
                    else:
                        p1.x = min(p1.x, dotinfo[0])
                        p2.x = max(p2.x, dotinfo[0])
                        p1.y = min(p1.y, dotinfo[1])
                        p2.y = max(p2.y, dotinfo[1])
            area = abs(p2.x - p1.x) * abs(p2.y - p1.y)
            key_lshoulder = person[2]
            key_rshoulder = person[5]
            mid_point  = 0.5 * (key_lshoulder[0] + key_rshoulder[0])
            print("person:%s, %s"%(str(person[2][0]), str(person[5][0])))
            print("sjf0213----index: %d, area: %f, mid_point: %f"%(index, area, mid_point))
            if area > areaMax:
                if key_lshoulder[2] > 0.1 and key_rshoulder[2] > 0.1 and mid_point > 0.25 * img_width and mid_point < 0.75 * img_width:
                    areaMax = area
                    personIndex = index
                    max_p1 = Point2D(p1)
                    max_p2 = Point2D(p2)
                    print("sjf0213---max, (%f, %f, %f, %f)"%(max_p1.x, max_p1.y, max_p2.x, max_p2.y))
            index = index + 1
        # print("sjf0213---3, (%f, %f, %f, %f)"%(max_p1.x, max_p1.y, max_p2.x, max_p2.y))

        x1 = int(round(max_p1.x))
        x2 = int(round(max_p2.x))
        y1 = int(round(max_p1.y))
        y2 = int(round(max_p2.y))
        print("sjf0213,x1=%s, x2=%s, y1=%s, y2=%s,"%(str(x1),str(x2),str(y1),str(y2)))

    # image source path
        source_path = os.path.split(imagePath)
        print("sjf0213---source_path: %s"%(str(source_path)))


    # hot keys image 
        # cv2.imshow("OpenPose 1.5.1 - Tutorial Python API", datum.cvOutputData)
        # cv2.waitKey(0)
        save_path = save_dir1 + source_path[1]
        cv2.imwrite(save_path,  datum.cvOutputData)
        print("sjf0213---saved: %s"%(save_path))

    # whole body image
        crop_img = imageToProcess[y1:y2, x1:x2]
        # cv2.imshow("cropped", crop_img)
        # cv2.waitKey(0)
        save_path = save_dir2 + source_path[1]
        cv2.imwrite(save_path, crop_img)
        print("sjf0213---saved: %s"%(save_path))

    # BOOBS image
        max_person = datum.poseKeypoints[personIndex]
        key_lshoulder = max_person[2]
        key_rshoulder = max_person[5]
        bp1 = Point2D(0.0, 0.0)
        bp2 = Point2D(0.0, 0.0)
        if key_lshoulder[2] > 0.1 and key_rshoulder[2] > 0.1:
            shoulder_width = abs(key_lshoulder[0] - key_rshoulder[0])
            bp1.x =  key_lshoulder[0] - (0.2 * shoulder_width)
            bp2.x =  key_rshoulder[0] + (0.2 * shoulder_width)
            bp1.y =  min(key_lshoulder[1], key_rshoulder[1])
            bp2.y =  bp1.y + (0.8 * shoulder_width) 
        print("sjf0213---boobs, (%f, %f, %f, %f)"%(bp1.x,bp2.x, bp1.y, bp2.y))
        boobs_img = imageToProcess[int(round(bp1.y)):int(round(bp2.y)), int(round(bp1.x)):int(round(bp2.x))]
        # cv2.imshow("boobs", boobs_img)
        # cv2.waitKey(0)
        save_path = save_dir3 + source_path[1]
        cv2.imwrite(save_path, boobs_img)
        print("sjf0213---saved: %s"%(save_path))
    end = time.time()
    print("OpenPose demo successfully finished. Total time: " + str(end - start) + " seconds")
except Exception as e:
    print(e)
    sys.exit(-1)
