# -*- coding: utf-8 -*-
# It requires OpenCV installed for Python
from __future__ import absolute_import, division, print_function, unicode_literals
import sys
import cv2
import os
from PIL import Image
from sys import platform
from random import shuffle
import numpy as np
import argparse
sys.path.append('/usr/local/python')
from openpose import pyopenpose as op
import tensorflow as tf
from tensorflow import keras
print("TensorFlow version is ", tf.__version__)
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
tf.enable_eager_execution()
from keras.models import Sequential
from keras.layers import *
from keras.optimizers import *
from tensorflow.python.keras.utils.data_utils import Sequence

IMAGE_SHAPE = (80, 140)
image_size = 160 # All images will be resized to 160x160
batch_size = 32
train_data_root = '/home/jufeng/stuff/images/train/'
train_datagen = tf.keras.preprocessing.image.ImageDataGenerator(rescale=1/255)
train_generator = train_datagen.flow_from_directory(str(train_data_root),
                target_size=(image_size, image_size),
                batch_size=batch_size,
                class_mode='binary')
test_data_root = '/home/jufeng/stuff/images/test/'
validation_datagen = tf.keras.preprocessing.image.ImageDataGenerator(rescale=1/255)
validation_generator = validation_datagen.flow_from_directory(str(test_data_root),
                target_size=(image_size, image_size),
                batch_size=batch_size,
                class_mode='binary')

for image_batch, label_batch in train_generator:
  print("Image batch shape: ", image_batch.shape)
  print("Label batch shape: ", label_batch.shape)
  break

x_train, y_train = train_generator[0]
print("x_train shape: ", x_train.shape)



IMG_SHAPE = (image_size, image_size, 3)

# Create the base model from the pre-trained model MobileNet V2
base_model = tf.keras.applications.MobileNetV2(input_shape=IMG_SHAPE,
                                               include_top=False,
                                               weights='imagenet')

base_model.trainable = False
# Let's take a look at the base model architecture
base_model.summary()


model = tf.keras.Sequential([
  base_model,
  keras.layers.GlobalAveragePooling2D(),
  keras.layers.Dense(1, activation='sigmoid')
])

model.compile(optimizer=tf.keras.optimizers.RMSprop(lr=0.0001),
              loss='binary_crossentropy',
              metrics=['accuracy'])

model.summary()
print(len(model.trainable_variables))

epochs = 10
steps_per_epoch = batch_size
validation_steps = batch_size
# with tf.device('/device:GPU:0'):
history = model.fit_generator(train_generator,
                              steps_per_epoch = steps_per_epoch,
                              epochs=epochs,
                              workers=4,
                              validation_data=validation_generator,
                              validation_steps=validation_steps)
# Creates a session with log_device_placement set to True.
sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
# Runs the op.
print(sess.run(history))