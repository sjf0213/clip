# -*- coding: utf-8 -*-
"""Download and Upload."""
from __future__ import unicode_literals
from datetime import datetime
import youtube_dl
import os
import argparse
# Imports the Google Cloud client library
from google.cloud import storage
# import google.cloud.storage
# Instantiates a client
BUCKET_NAME = 'songjufeng-storage1'

# demo url:'gs://songjufeng-storage1/smallvideo.mp4'
def sh(script):
    os.system("bash -c '%s'" % script)

def splitall(path):
    allparts = []
    while 1:
        parts = os.path.split(path)
        if parts[0] == path:  # sentinel for absolute paths
            allparts.insert(0, parts[0])
            break
        elif parts[1] == path: # sentinel for relative paths
            allparts.insert(0, parts[1])
            break
        else:
            path = parts[0]
            allparts.insert(0, parts[1])
    return allparts

def download():
    # Get the Person Info in Video
    video_url = os.path.dirname(args.url)
    print('--- 1.Download video from Youtube to local---')
    print("Now Time:", datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
    ydl_opts = {}# 'nocheckcertificate':True
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        ydl.download([video_url])
    script  = 'youtube-dl -v %s'%(video_url)
    print (script)
    sh(script)
    return

def upload_blob(bucket_name, source_file_name, destination_blob_name):
    """Uploads a file to the bucket."""
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)
    blob.upload_from_filename(source_file_name)
    print('Local Source File: {} \nTarget Localtion: {}.'.format(
        source_file_name,
        destination_blob_name))

if __name__=="__main__":
    print('---begin upload---{}'.format(datetime.now().strftime("%d/%m/%Y %H:%M:%S")))
    a = argparse.ArgumentParser()
    a.add_argument("--source", help="Local file path")
    args = a.parse_args()
    print(args)

    pathlist = splitall(args.source)
    # 保留上传source path的文件名和上一层目录
    target_file = os.path.join(pathlist[len(pathlist)-2], pathlist[len(pathlist)-1])
    target_file = os.path.join('videos',target_file)
    upload_blob(BUCKET_NAME, args.source, target_file)

    print('---end upload---{}'.format(datetime.now().strftime("%d/%m/%Y %H:%M:%S")))
