# -*- coding: utf-8 -*-
"""Object Tracking."""
import sys
import os
import errno
from datetime import datetime
import json
import argparse
import cv2
from google.cloud import videointelligence_v1p2beta1 as videointelligence
from google.protobuf.json_format import MessageToJson
from google.protobuf.json_format import MessageToDict


# datetime object containing current date and time
	

SNAPSHOT_PATH= 'shot'
SEGINFO_PATH = 'info'
OBJECT_TRACKING_JSON = 'objectTrack.json'
PERSON_TRACKING_JSON = 'personTrack.json'

print('OpenCV version: %s'% cv2.__version__)

# Utils
def printAnnoInfo(object_annotation):
    entity_desc = object_annotation.entity.description
    print('Entity description: {}'.format(entity_desc))
    if object_annotation.entity.entity_id:
        print('Entity id: {}'.format(object_annotation.entity.entity_id))
    print('Segment: {}s to {}s'.format(
        object_annotation.segment.start_time_offset.seconds +
        object_annotation.segment.start_time_offset.nanos / 1e9,
        object_annotation.segment.end_time_offset.seconds +
        object_annotation.segment.end_time_offset.nanos / 1e9))
    print('Confidence: {}'.format(object_annotation.confidence))

    print('Frame counts: {}'.format(len(object_annotation.frames)))
    frame_list = []
    frame0 = object_annotation.frames[0]
    frame1 = object_annotation.frames[len(object_annotation.frames) - 1]
    frame_list.append(frame0)
    frame_list.append(frame1)
    for one_frame in frame_list:
        box = one_frame.normalized_bounding_box
        print('Time offset of the first frame: {}s'.format(
            one_frame.time_offset.seconds + one_frame.time_offset.nanos / 1e9))
        print('Bounding box position:')
        print('\tleft  : {}'.format(box.left))
        print('\ttop   : {}'.format(box.top))
        print('\tright : {}'.format(box.right))
        print('\tbottom: {}'.format(box.bottom))
        print('\n')

def saveJsonStrInPath(fullpath, strobj):
    dirname = os.path.dirname(fullpath)
    if not os.path.exists(dirname):
        try:
            os.makedirs(dirname)
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise

    print('---save annotations info in a json file---:%s'%(fullpath))
    with open(fullpath, "w") as f:
        f.write(strobj)
        f.close() 


# Invoke Goolge Cloud Platform Video Intelligence Api
def extractObjectTrackFromVideo(inputUri):
    # It is recommended to use location_id as 'us-east1' for the best latency
    # due to different types of processors used in this region and others.
    video_client = videointelligence.VideoIntelligenceServiceClient()
    features = [videointelligence.enums.Feature.OBJECT_TRACKING]
    operation = video_client.annotate_video(
        input_uri=inputUri, features=features)
        # input_uri=inputUri, features=features, location_id='us-east1')
    print('\nProcessing video for object annotations.')
    result = operation.result(timeout=30000)
    print('\nFinished processing.\n')
    return result
    
def filterObjFromAnaliseResult(analiseResult):
    # The first result is retrieved because a single video was processed.
    object_annotations = analiseResult.annotation_results[0].object_annotations
    print('Entity counts: {}'.format(len(object_annotations)))

    # filter all segment that is marked as "person", and more than 10 frames
    person_seg_info_list = []
    for object_annotation in object_annotations:
        entity_desc = object_annotation.entity.description
        confidence = object_annotation.confidence
        if entity_desc == 'person' and confidence > 0.9 and len(object_annotation.frames) > 10:
            person_seg_dict = MessageToDict(object_annotation, preserving_proto_field_name = True)
            person_seg_info_list.append(person_seg_dict)

    print('persion anno count:%s', len(person_seg_info_list))
    # save as file
    person_json_str = json.dumps(person_seg_info_list, indent=4, sort_keys=True)
    return person_json_str
    
# Make Snapshot
def extractImagesByTimeOffset(localVideoPath, timepoint):
    vidcap = cv2.VideoCapture(localVideoPath)
    success,image = vidcap.read()
    success = True
    vidcap.set(cv2.CAP_PROP_POS_MSEC,(timepoint*1000))    # added this line 
    success,image = vidcap.read()

    # 创建video下的shot目录
    video_dir = os.path.dirname(args.localPath)
    shot_dir = os.path.join(video_dir, SNAPSHOT_PATH)
    if not os.path.exists(shot_dir):
        try:
            os.makedirs(shot_dir)
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
    # print ('Read a new frame: ', success)
    filename = "frame" + str(timepoint)
    filepath = os.path.join(shot_dir, filename + '.jpg')
    # print(filepath)
    cv2.imwrite(filepath,image) # save frame as JPEG file

# Analise Data and find keep useful date
def fetchSnapshotInVideo(localVideoPath, personTrackJsonPath):
    with open(personTrackJsonPath) as f:
        person_seg_list = json.load(f)
    # print(person_seg_list)
    seg_index = 0
    for seg_dict in person_seg_list:
        max_area = 0
        seg_index += 1
        frame_chosen = seg_dict['frames'][0]
        for frame in seg_dict['frames']:
            box = frame['normalized_bounding_box']
            # print(box)
            if 'top' in box and 'bottom' in box and 'left' in box and 'right' in box:
                midline = (float(box['left']) + float(box['right'])) * 0.5
                width = float(box['right']) - float(box['left'])
                hieght = float(box['bottom']) - float(box['top'])
                area = abs(width * hieght)
                frame['area'] = area
            else:
                frame['area'] = 0
            if area > max_area:
                max_area = area
                frame_chosen = frame
        if max_area > 0.1:
            # print(frame_chosen)
            extractImagesByTimeOffset(localVideoPath, float(frame_chosen['time_offset'].strip('s')))
    # for object_annotation in object_annotations:


# demo url:'gs://songjufeng-storage1/smallvideo.mp4'
if __name__=="__main__":
    print('---begin---:{}'.format(datetime.now().strftime("%d/%m/%Y %H:%M:%S")))
    a = argparse.ArgumentParser()
    a.add_argument("--inputUri", help="uri to video")
    a.add_argument("--localPath", help="path to local video")
    args = a.parse_args()
    print(args)

    # Get the Person Info in Video
    video_dir = os.path.dirname(args.localPath)

    print('--- 1.Google Cloud Platform video intelligence API ---')
    print("Now Time:", datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
    # Invoke Google Cloud Video Intelligence Api: Object Tracking, save json
    objectTrackingResult = extractObjectTrackFromVideo(args.inputUri)
    result_dict = MessageToDict(objectTrackingResult, preserving_proto_field_name = True)
    result_json_str = json.dumps(result_dict, indent=4, sort_keys=True)
    info_dir = os.path.join(video_dir, SEGINFO_PATH)
    object_track_json_name = os.path.join(info_dir, OBJECT_TRACKING_JSON)
    saveJsonStrInPath(object_track_json_name, result_json_str)

    # find person in object tracking result
    print('--- 2.Find person in object tracking result ---')
    print("Now Time:", datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
    person_segments_info_str = filterObjFromAnaliseResult(objectTrackingResult)
    person_track_json_name = os.path.join(info_dir, PERSON_TRACKING_JSON)
    saveJsonStrInPath(person_track_json_name, person_segments_info_str)

    # Get Snapshot in video According to filtered json data
    print('--- 3.Make snapshot for each segment---')
    print("Now Time:", datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
    fetchSnapshotInVideo(args.localPath, person_track_json_name)

    print('---end---:{}'.format(datetime.now().strftime("%d/%m/%Y %H:%M:%S")))

