# -*- coding: utf-8 -*-
# It requires OpenCV installed for Python
from __future__ import absolute_import, division, print_function, unicode_literals
import sys
import cv2
import os
import errno
import argparse
import markpose

# Flags
print("sjf0213---parse args")
parser = argparse.ArgumentParser()
parser.add_argument("--input", default="/home/jufeng/codes/clip/testdata/testsmall.mp4", help="video dir")
args = parser.parse_known_args()

import scenedetect
from scenedetect.video_manager import VideoManager
from scenedetect.scene_manager import SceneManager
from scenedetect.frame_timecode import FrameTimecode
from scenedetect.stats_manager import StatsManager
# from scenedetect.video_splitter import video_splitter
# from scenedetect.detectors import ContentDetector
from scenedetect.detectors.content_detector import ContentDetector

def check_and_make_dir(dir):
    if not os.path.exists(dir):
        try:
            os.makedirs(dir)
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise

def save_video_capture(video, capture_dir):
    vidcap = cv2.VideoCapture(video)
    success,image = vidcap.read()
    count = 0
    while success:
        image_path = os.path.join(capture_dir, "frame%d.jpg" % count)
        cv2.imwrite(image_path, image)     # save frame as JPEG file      
        success,image = vidcap.read()
        print('Read a new frame: %r， %d'%(success, count))
        count += 1

def split_scenes(video_path):
    # type: (str) -> List[Tuple[FrameTimecode, FrameTimecode]]
    video_manager = VideoManager([video_path])
    stats_manager = StatsManager()
    # Construct our SceneManager and pass it our StatsManager.
    scene_manager = SceneManager(stats_manager)

    # Add ContentDetector algorithm (each detector's constructor
    # takes detector options, e.g. threshold).
    scene_manager.add_detector(ContentDetector())
    base_timecode = video_manager.get_base_timecode()

    # We save our stats file to {VIDEO_PATH}.stats.csv.
    stats_file_path = '%s.stats.csv' % video_path

    scene_list = []

    try:
        # If stats file exists, load it.
        if os.path.exists(stats_file_path):
            # Read stats from CSV file opened in read mode:
            with open(stats_file_path, 'r') as stats_file:
                stats_manager.load_from_csv(stats_file, base_timecode)

        # Set downscale factor to improve processing speed.
        video_manager.set_downscale_factor()

        # Start video_manager.
        video_manager.start()

        # Perform scene detection on video_manager.
        scene_manager.detect_scenes(frame_source=video_manager)

        # Obtain list of detected scenes.
        scene_list = scene_manager.get_scene_list(base_timecode)
        # Each scene is a tuple of (start, end) FrameTimecodes.

        print('List of scenes obtained:')
        for i, scene in enumerate(scene_list):
            print(
                '--------Scene %2d: Start %s / Frame %d, End %s / Frame %d' % (
                i+1,
                scene[0].get_timecode(), scene[0].get_frames(),
                scene[1].get_timecode(), scene[1].get_frames(),))
            pair = os.path.split(video_path)
            video_dir = pair[0]
            video_filename = os.path.splitext(pair[1])[0]
            # make meta dir 
            video_meta_dir = os.path.join(video_dir, video_filename)
            check_and_make_dir(video_meta_dir)
            # make foler for every scene
            scene_dir = os.path.join(video_meta_dir, 'scene%d'%(i+1))
            check_and_make_dir(scene_dir)
            # name every scene and split them
            scene_video_path = os.path.join(scene_dir, 'scene.mp4')
            duration = scene[1] - scene[0]
            cmdline = 'ffmpeg -ss %s  -i %s -c copy -t %s %s'%(scene[0].get_timecode(), video_path, duration.get_timecode(), scene_video_path)
            os.system("bash -c '%s'" % cmdline)
            print('--------------------------------------------')

            # 对每一个scene的每一帧截图保存
            capture_dir = os.path.join(scene_dir, 'capture')
            check_and_make_dir(capture_dir)
            save_video_capture(scene_video_path, capture_dir)

            # mark every pose and get the boobs
            markpose.markpose(capture_dir)

    finally:
        video_manager.release()

    return scene_list



targetvideo = args[0].input
# 按照镜头切换分割scene
split_scenes(targetvideo)






