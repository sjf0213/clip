# -*- coding: utf-8 -*-
"""Download and Upload."""
from __future__ import unicode_literals
from datetime import datetime
import youtube_dl
import os
import argparse
import pafy

def sh(script):
    os.system("bash -c '%s'" % script)

def splitall(path):
    allparts = []
    while 1:
        parts = os.path.split(path)
        if parts[0] == path:  # sentinel for absolute paths
            allparts.insert(0, parts[0])
            break
        elif parts[1] == path: # sentinel for relative paths
            allparts.insert(0, parts[1])
            break
        else:
            path = parts[0]
            allparts.insert(0, parts[1])
    return allparts

def download_sh(video_url):
    script  = 'youtube-dl -v %s'%(video_url)
    print (script)
    sh(script)
    return

def mycb(total, recvd, ratio, rate, eta):
    print(recvd, ratio, eta)

def download(video_url):
    # ydl_opts = {}# 'nocheckcertificate':True
    # with youtube_dl.YoutubeDL(ydl_opts) as ydl:
    #     ydl.download([video_url])
    vid = pafy.new(video_url)
    print(vid.title)

    # s = vid.getbest()
    # print("Size is %s" % s.get_filesize())
    # filename = s.download(filepath = 'demo.mp4',quiet=True, callback=mycb)  # starts download
    # print('---end download 0---{}'.format(datetime.now().strftime("%d/%m/%Y %H:%M:%S")))

if __name__=="__main__":
    print('---begin download---{}'.format(datetime.now().strftime("%d/%m/%Y %H:%M:%S")))
    a = argparse.ArgumentParser()
    a.add_argument("--url", help="uri to video")
    args = a.parse_args()
    print(args)

    download(args.url)

    print('---end download---{}'.format(datetime.now().strftime("%d/%m/%Y %H:%M:%S")))

