# -*- coding: utf-8 -*-
# It requires OpenCV installed for Python
import sys
import cv2
import os
import argparse

# Flags
parser = argparse.ArgumentParser()
parser.add_argument("--dir", default="/home/jufeng/stuff/images/boobs/yes/", help="Process a directory of images. Read all standard formats (jpg, png, bmp, etc..)")
parser.add_argument("--mark", default="yes", help="yes or no")
args = parser.parse_known_args()

process_dir = args[0].dir
mark_txt = args[0].mark
img_list = os.listdir(process_dir)
print("the dir to process have image count:%d"%(len(img_list)))

first_img = img_list[0]

mark_txt = '.'+mark_txt
for img in img_list:
    
    sourcename = os.path.splitext(img)
    newname = process_dir + sourcename[0] + mark_txt + sourcename[1]
    oldname = process_dir + img
    print('oldname:%s'%(oldname))
    print('newname:%s'%(newname))
    os.rename(oldname, newname)